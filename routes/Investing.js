var express = require('express');
var router = express.Router();
var InvestingAddModel= require('../Schema/Investing_table');
var AdminModel= require('../Schema/Admin_table');
/* GET home page. */


 router.get('/upload', function(req,res,next){
res.render('upload-file',{title:'Upload File', success:''})

 });

 
//for image




//Add Investing
router.get('/add', function(req, res, next) {
    res.render('Investing_add');
});
router.post('/add',function(req,res,next){
    console.log(req.body);
    const investing_data={
         video           :     req.body.video,
        description           :     req.body.description,
        description2     :     req.body.description2,
        consistenttxt1           :     req.body.ctxt1 ,
        consistenttxt2           :     req.body.ctxt2 ,
        sustaintxt1           :     req.body.stxt1 ,
        sustaintxt2           :     req.body.stxt2 ,
        robusttxt1           :     req.body.rtxt1 ,
        robusttxt2         :     req.body.rtxt2 ,
        image_upload      :     req.body.image
    }
    var investingdata=InvestingAddModel(investing_data);  
    investingdata.save(function(err){
        if(err)
        console.log(err);
        else
        res.redirect('add');
        });
    });
    
    //display data
    router.get('/data_display',function(req,res,next){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        InvestingAddModel.find(function(err,db_investing_array){
            if(err)
            console.log("Error");
            else
            {
            console.log(db_investing_array);
            res.render('Investing_display',{investing_array:db_investing_array});
            }
        });
    });
    //display data
    
    //delete data
    router.get('/delete/:id',function(req,res){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        InvestingAddModel.findByIdAndDelete(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error in delete data");
            else
            {
                console.log(db_investing_array);
                res.redirect('/investing/data_display');
            }
        });
    });
    //delete data
    //edit data
    router.get('/edit/:id',function(req,res,next){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        InvestingAddModel.findById(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error");
            else
            {
                console.log("before edit : ",db_investing_array);
                res.render('Investing_edit',{investing_array:db_investing_array});
            }
        });
    });
    router.post('/edit/:id',function(req,res){
        console.log("Edit ID is"+req.params.id);
            const investing_data= {
                video           :     req.body.video,
                description           :     req.body.description,
                description2     :     req.body.description2,
                consistenttxt1           :     req.body.ctxt1 ,
                consistenttxt2           :     req.body.ctxt2 ,
                sustaintxt1           :     req.body.stxt1 ,
                sustaintxt2           :     req.body.stxt2 ,
                robusttxt1           :     req.body.rtxt1 ,
                robusttxt2         :     req.body.rtxt2 ,
                image_upload      :     req.body.image
            }
        InvestingAddModel.findByIdAndUpdate(req.params.id,investing_data,function(err){
                if(err)
                {
                console.log(req.params.id);
                    console.log("Error in Record Update");
                }
            else
                res.redirect('/investing/data_display');
            });
        });    
    //edit data
    //single-record
    router.get('/show/:id',function(req,res){
        var mysessionvalue= req.session.Admin_email;
        if(!mysessionvalue){
            res.redirect('/');
        }
        console.log(req.params.id);
        InvestingAddModel.findById(req.params.id,function(err,db_investing_array){
            if(err)
            console.log("Error in single Record Fetch");
            else
            {
                console.log(db_investing_array);
                res.render('Investing_singledata',{investing_array:db_investing_array});
            }
        })
    })
    //single-record
    module.exports = router;    
    